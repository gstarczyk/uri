<?php

namespace Gstarczyk\Uri;

class Uri
{
    /**
     * @var string
     */
    private $scheme;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string[] map string => string
     */
    private $query;

    /**
     * @var string
     */
    private $fragment;

    public function __construct(
        string $scheme = null,
        string $username = null,
        string $password = null,
        string $host = null,
        int $port = null,
        string $path = null,
        array $query = [],
        string $fragment = null
    ) {
        $this->scheme = $scheme;
        $this->username = $username;
        $this->password = $password;
        $this->host = $host;
        $this->port = $port;
        $this->path = $path;
        $this->query = $query;
        $this->fragment = $fragment;
    }

    public function getScheme(): ?string
    {
        return $this->scheme;
    }

    public function getUserInfo(): ?string
    {
        $userInfo = $this->username;
        if (is_string($this->username) && is_string($this->password)) {
            $userInfo = $this->username.':'.$this->password;
        }

        return $userInfo;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function getPort(): ?int
    {
        return $this->port;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function getSlug(): ?string
    {
        if ($this->path === null || empty($this->path)) {
            return null;
        }
        if (substr($this->path, -1, 1) === '/') {
            return null;
        }
        $parts = explode('/', $this->path);

        return array_pop($parts);
    }

    /**
     * @return string[]
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    public function getFragment(): ?string
    {
        return $this->fragment;
    }

    public function getParts(): array
    {
        return [
            'scheme' => $this->scheme,
            'user' => $this->username,
            'pass' => $this->password,
            'host' => $this->host,
            'port' => $this->port,
            'path' => $this->path,
            'query' => $this->query,
            'fragment' => $this->fragment,
        ];
    }

    public function __toString(): string
    {
        return $this->serializeUri();
    }

    /**
     * Return serialized uri without password
     * @return string
     */
    public function serializeUri(): string
    {
        return $this->serializeScheme()
            .$this->serializeAuthority()
            .$this->path
            .$this->serializeQuery()
            .$this->serializeFragment();
    }

    /**
     * Return serialized uri with password
     * @return string
     */
    public function serializeFullUri(): string
    {
        return $this->serializeScheme()
            .$this->serializeFullAuthority()
            .$this->path
            .$this->serializeQuery()
            .$this->serializeFragment();
    }

    private function serializeScheme(): string
    {
        if (empty($this->scheme)) {
            $scheme = '';
        } else {
            $scheme = $this->scheme.':';
        }

        return $scheme;
    }

    private function serializeAuthority(): string
    {
        $authority = '';
        if ($this->username !== null) {
            $authority .= urlencode($this->username).'@';
        }

        if ($this->host !== null) {
            $authority .= $this->host;
        }
        if ($this->port !== null) {
            $authority .= ':'.$this->port;
        }
        if (!empty($authority)) {
            $authority = '//'.$authority;
        }

        return $authority;
    }

    private function serializeFullAuthority(): string
    {
        $authority = '';
        if ($this->username !== null) {
            $authority .= urlencode($this->username);
            if ($this->password !== null) {
                $authority .= ':'.urlencode($this->password);
            }
            $authority .= '@';
        }

        if ($this->host !== null) {
            $authority .= $this->host;
        }
        if ($this->port !== null) {
            $authority .= ':'.$this->port;
        }
        if (!empty($authority)) {
            $authority = '//'.$authority;
        }

        return $authority;
    }

    private function serializeQuery(): string
    {
        $query = http_build_query($this->query);
        if (!empty($query)) {
            $query = '?'.$query;
        }

        return $query;
    }

    private function serializeFragment(): string
    {
        if (!empty($this->fragment)) {
            return '#'.$this->fragment;
        }

        return '';
    }
}

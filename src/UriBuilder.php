<?php

namespace Gstarczyk\Uri;

class UriBuilder
{
    /**
     * @var PathResolver
     */
    private $pathResolver;

    /**
     * @var string
     */
    private $scheme;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string[] map string => string
     */
    private $query = [];

    /**
     * @var string
     */
    private $fragment;

    public function __construct(PathResolver $pathResolver)
    {
        $this->pathResolver = $pathResolver;
    }

    public static function buildUri(): self
    {
        return new UriBuilder(
            new PathResolver()
        );
    }

    public function withPartsFromStringUri(string $uri): self
    {
        $keys = ['scheme', 'user', 'pass', 'host', 'port', 'path', 'query', 'fragment'];
        $parts = parse_url($uri);
        $merged = array_merge(array_fill_keys($keys, null), $parts);

        $query = [];
        parse_str($merged['query'], $query);

        $this->scheme = $merged['scheme'];
        $this->username = $merged['user'];
        $this->password = urldecode($merged['pass']);
        $this->host = $merged['host'];
        $this->port = $merged['port'];
        $this->path = $merged['path'];
        $this->query = $query;
        $this->fragment = $merged['fragment'];

        return $this;
    }

    public function withPartsFromUriObject(Uri $uri): self
    {
        $merged = $uri->getParts();
        $this->scheme = $merged['scheme'];
        $this->username = $merged['user'];
        $this->password = $merged['pass'];
        $this->host = $merged['host'];
        $this->port = $merged['port'];
        $this->path = $merged['path'];
        $this->query = $merged['query'];
        $this->fragment = $merged['fragment'];

        return $this;
    }

    public function withScheme(string $scheme): self
    {
        $this->scheme = $scheme;

        return $this;
    }

    public function withoutScheme(): self
    {
        $this->scheme = null;

        return $this;
    }

    public function withUserInfo(string $username, string $password = null): self
    {
        $this->username = $username;
        $this->password = $password;

        return $this;
    }

    public function withoutUserInfo(): self
    {
        $this->username = null;
        $this->password = null;

        return $this;
    }

    public function withoutPassword(): self
    {
        $this->password = null;

        return $this;
    }

    public function withHost(string $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function withoutHost(): self
    {
        $this->host = null;

        return $this;
    }

    public function withPort(int $port): self
    {
        $this->port = $port;

        return $this;
    }

    public function withoutPort(): self
    {
        $this->port = null;

        return $this;
    }

    public function withPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @param string $path
     *
     * @return UriBuilder
     * @throws PathResolverException
     */
    public function withResolvedPath(string $path): self
    {
        $this->path = $this->pathResolver->resolvePath($path);

        return $this;
    }

    /**
     * @param string $path
     *
     * @return UriBuilder
     * @throws PathResolverException
     */
    public function withExistingPathResolvedWith(string $path): self
    {
        $this->path = $this->pathResolver->resolvePaths($this->path, $path);

        return $this;
    }

    public function withoutPath(): self
    {
        $this->path = null;

        return $this;
    }

    /**
     * @param string $slug
     *
     * @return UriBuilder
     * @throws SlugException
     */
    public function withSlug(string $slug): self
    {
        if (empty($this->path)) {
            throw new SlugException('Cannot define slug on empty path');
        }
        if (substr($this->path, -1, 1) === '/') {
            $this->path .= $slug;
        } else {
            $parts = explode('/', $this->path);
            array_pop($parts);//remove existing slug
            $parts[] = $slug;
            $this->path = implode('/', $parts);
        }

        return $this;
    }

    public function withoutSlug(): self
    {
        if (empty($this->path)) {
            return $this;
        }
        $parts = explode('/', $this->path);
        array_pop($parts);//remove existing slug
        $this->path = implode('/', $parts).'/';

        return $this;
    }

    public function withQuery(array $query): self
    {
        $this->query = $query;

        return $this;
    }

    public function withoutQuery(): self
    {
        $this->query = [];

        return $this;
    }

    public function withFragment(string $fragment): self
    {
        $this->fragment = $fragment;

        return $this;
    }

    public function withoutFragment(): self
    {
        $this->fragment = null;

        return $this;
    }

    public function getUri(): Uri
    {
        return new Uri(
            $this->scheme,
            $this->username,
            $this->password,
            $this->host,
            $this->port,
            $this->path,
            $this->query,
            $this->fragment
        );
    }
}

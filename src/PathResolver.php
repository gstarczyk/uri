<?php

namespace Gstarczyk\Uri;

/**
 * Resolve dots (".", "..") in path
 */
class PathResolver
{
    /**
     * @param string $path
     *
     * @return string
     * @throws PathResolverException
     */
    public function resolvePath(string $path): string
    {
        $parts = explode('/', $path);
        $result = [];
        foreach ($parts as $part) {
            if ($part == '.') {
                continue;
            } elseif ($part == '..') {
                $removed = array_pop($result);
                if (empty($result) || $removed == '') {
                    throw new PathResolverException('Cannot resolve "..", path is empty');
                }
            } else {
                $result[] = $part;
            }
        }

        return implode('/', $result);
    }

    /**
     * @param string $basePath
     * @param string $path
     *
     * @return string
     * @throws PathResolverException
     */
    public function resolvePaths(string $basePath, string $path): string
    {
        if ($this->isAbsolute($path)) {
            return $path;
        }

        return $this->resolvePath(
            $this->removeSlug($basePath).$path
        );
    }

    private function isAbsolute(string $path): bool
    {
        if (substr($path, 0, 1) !== '/') {
            return false;
        }
        if ($this->hasDots($path)) {
            return false;
        }

        return true;
    }

    private function hasDots(string $path): bool
    {
        $parts = explode('/', $path);
        foreach ($parts as $part) {
            if (in_array($part, ['.', '..'])) {
                return true;
            }
        }

        return false;
    }

    private function hasSlug(string $path): bool
    {
        return substr($path, -1, 1) !== '/';
    }

    private function removeSlug(string $path): string
    {
        if (!$this->hasSlug($path)) {
            return $path;
        }
        $parts = $this->getParts($path);
        array_pop($parts);

        return implode('/', $parts).'/';
    }

    private function getParts(string $path): array
    {
        return explode('/', $path);
    }
}

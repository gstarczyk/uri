# Immutable Uri manipulation library

Library that allow easy build and create immutable Uri.

## Build

To make creation process convenient as possible, simple Uri builder is provided
with this library.

### build URI from parts

With builder you can easily build you uri, defining each part of uri separately:

    $uri = UriBuilder::buildUri()
        ->withScheme('http')
        ->withUserInfo('user', 'pass')
        ->withHost('example.org')
        ->withPort(8080)
        ->withPath('/path/to/file.png')
        ->withQuery(['key' => 'value'])
        ->withFragment('top')
        ->getUri();

### build URI from string
        
Of course you can use builder to create Uri from string:

    $stringUri = 'https://example.org/my/page?sidebar=left@section1';
    
    $uri = UriBuilder::buildUri()
        ->withPartsFromStringUri($stringUri)
        ->getUri();

If you need to modify existing Uri you have to create new one with modification

    $stringUri = 'https://example.org/my/page?sidebar=left@section1';

    $uri = UriBuilder::buildUri()
        ->withPartsFromStringUri($stringUri)
        ->withoutQuery()
        ->withoutFragment()
        ->withUserInfo('someuser')
        ->getUri();

### build URI from existing URI

You can build URI from another existing URI:

    $uri = UriBuilder::buildUri()
        ->withPartsFromUriObject($exitingUri)
        ->withoutQuery()
        ->getUri();


### define slug

Some times you want change only part of path. 
To help with such case UriBuilder has support for slug manipulation.

    $stringUri = 'https://example.org/my/cover.png';
    
    $uri = UriBuilder::buildUri()
        ->withPartsFromStringUri($stringUri)
        ->withSlug('poster.jpg')
        ->getUri();
     
     // $uri => https://example.org/my/poster.jpg

or if you want only remove slug from path:

    $stringUri = 'https://example.org/my/cover.png';
    
    $uri = UriBuilder::buildUri()
        ->withPartsFromStringUri($stringUri)
        ->withoutSlug()
        ->getUri();
     
     // $uri => https://example.org/my/
     
remember that slug is last segment of path:

    $stringUri = 'https://example.org/my/images';
    // "images" from path will be treated as slug 
    
    $uri = UriBuilder::buildUri()
        ->withPartsFromStringUri($stringUri)
        ->withoutSlug()
        ->getUri();
     
     // $uri => https://example.org/my/

### resolving paths

You can build you "path" using relative path segments

    $uri = UriBuilder::buildUri()
        ->withResolvedPath('/home/user1/documents/../downloads/./file1')
        ->getUri();
     
     // $uri->getPath => /home/user1/downloads/file1

or you can build path by combining absolute base path with many relative paths

    $uri = UriBuilder::buildUri()
        ->withPath('/home/user1/')
        ->withExistingPathResolvedWith('../user2/documents')
        ->withExistingPathResolvedWith('../downloads/file1.ext')
        ->getUri();
     
     // $uri->getPath => /home/user2/downloads/file1.ext

Remember that path you build must be resolvable.
For example cannot start with "../" or 

    //invalid base path
    $uri = UriBuilder::buildUri()
        ->withResolvedPath('../home/user1/documents/file1')
        ->getUri();
        
    //invalid base path
    $uri = UriBuilder::buildUri()
        ->withPath('../home/user1/')
        ->withExistingPathResolvedWith('./documents')
        ->getUri();
    
    //invalid path to resolve with
    $uri = UriBuilder::buildUri()
        ->withPath('/home/user1/')
        ->withExistingPathResolvedWith('../../../../user2/documents')
        ->getUri();
    
    // each above builders throw exception

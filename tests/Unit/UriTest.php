<?php

namespace Gstarczyk\Uri\Tests\Unit;

use Gstarczyk\Uri\UriBuilder;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class UriTest extends TestCase
{
    public function testSerializingUri()
    {
        $uri = UriBuilder::buildUri()
            ->withScheme('http')
            ->withUserInfo('user', 'pass')
            ->withHost('example.org')
            ->withPort(8080)
            ->withPath('/path/to/file.png')
            ->withQuery(['key' => 'value'])
            ->withFragment('top')
            ->getUri();

        $serialized = $uri->serializeUri();

        $expected = 'http://user@example.org:8080/path/to/file.png?key=value#top';
        Assert::assertEquals($expected, $serialized);
    }

    public function testSerializingUriWithoutAuthority()
    {
        $uri = UriBuilder::buildUri()
            ->withScheme('file')
            ->withPath('/path/to/file.png')
            ->withQuery(['key' => 'value'])
            ->withFragment('top')
            ->getUri();

        $serialized = $uri->serializeUri();

        $expected = 'file:/path/to/file.png?key=value#top';
        Assert::assertEquals($expected, $serialized);
    }

    public function testSerializingFullUriWithAuthorityContainingIllegalCharacters()
    {
        $uri = UriBuilder::buildUri()
            ->withScheme('http')
            ->withUserInfo('Y+30@Wz/hd', 'Y+30@Wz/hd')
            ->withHost('example.org')
            ->withPath('/path/to/file.png')
            ->getUri();

        $serialized = $uri->serializeFullUri();

        $expected = 'http://Y%2B30%40Wz%2Fhd:Y%2B30%40Wz%2Fhd@example.org/path/to/file.png';
        Assert::assertEquals($expected, $serialized);
    }

    public function testSerializingUriWithoutSchemeAndHost()
    {
        $uri = UriBuilder::buildUri()
            ->withPath('/path/to/file.png')
            ->getUri();

        $serialized = $uri->serializeUri();

        $expected = '/path/to/file.png';
        Assert::assertEquals($expected, $serialized);
    }

    public function testSerializingUriWithoutScheme()
    {
        $uri = UriBuilder::buildUri()
            ->withHost('example.org')
            ->withPath('/path/to/file.png')
            ->getUri();

        $serialized = $uri->serializeUri();

        $expected = '//example.org/path/to/file.png';
        Assert::assertEquals($expected, $serialized);
    }

    public function testSerializingFullUriWithoutSchemeAndHost()
    {
        $uri = UriBuilder::buildUri()
            ->withPath('/path/to/file.png')
            ->getUri();

        $serialized = $uri->serializeFullUri();

        $expected = '/path/to/file.png';
        Assert::assertEquals($expected, $serialized);
    }

    public function testSerializingFullUriWithoutScheme()
    {
        $uri = UriBuilder::buildUri()
            ->withHost('example.org')
            ->withPath('/path/to/file.png')
            ->getUri();

        $serialized = $uri->serializeFullUri();

        $expected = '//example.org/path/to/file.png';
        Assert::assertEquals($expected, $serialized);
    }

    /**
     * @param string $path
     * @param string $expected
     *
     * @dataProvider slugDataProvider
     */
    public function testGetSlugReturnLastPartOfPath(string $path, string $expected)
    {
        //given
        $uri = UriBuilder::buildUri()
            ->withPath($path)
            ->getUri();

        //when
        $result = $uri->getSlug();

        //then
        Assert::assertEquals($expected, $result);
    }

    public function slugDataProvider()
    {
        return [
            'file' => [
                'path' => '/path/to/file.png',
                'slug' => 'file.png',
            ],
            'dir' => [
                'path' => '/path/to/dir',
                'slug' => 'dir',
            ],
        ];
    }

    public function testGetSlugReturnNullIfLastCharOfPathIsSlash()
    {
        //given
        $uri = UriBuilder::buildUri()
            ->withPath('/path/to/')
            ->getUri();

        //when
        $result = $uri->getSlug();

        //then
        Assert::assertNull($result);
    }

    public function testGetSlugReturnNullIfNoPathIsDefined()
    {
        //given
        $uri = UriBuilder::buildUri()
            ->getUri();

        //when
        $result = $uri->getSlug();

        //then
        Assert::assertNull($result);
    }
}

<?php

namespace Gstarczyk\Uri\Tests\Unit;

use Gstarczyk\Uri\PathResolverException;
use Gstarczyk\Uri\SlugException;
use Gstarczyk\Uri\UriBuilder;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class UriBuilderTest extends TestCase
{
    public function testBuilderWithScheme()
    {
        $uri = UriBuilder::buildUri()
            ->withScheme('s3')
            ->getUri();

        Assert::assertEquals('s3', $uri->getScheme());
    }

    public function testClearingScheme()
    {
        $uri = UriBuilder::buildUri()
            ->withScheme('s3')
            ->withoutScheme()
            ->getUri();

        Assert::assertNull($uri->getScheme());
    }

    public function testBuilderWithFullUserInfo()
    {
        $uri = UriBuilder::buildUri()
            ->withUserInfo('username', 'password')
            ->getUri();

        Assert::assertEquals('username', $uri->getUsername());
        Assert::assertEquals('password', $uri->getPassword());
    }

    public function testBuilderWithUserInfoWithoutPassword()
    {
        $uri = UriBuilder::buildUri()
            ->withUserInfo('username')
            ->getUri();

        Assert::assertEquals('username', $uri->getUsername());
    }

    public function testClearingUserInfo()
    {
        $uri = UriBuilder::buildUri()
            ->withUserInfo('username', 'password')
            ->withoutUserInfo()
            ->getUri();

        Assert::assertNull($uri->getUserInfo());
    }

    public function testClearingPassword()
    {
        $uri = UriBuilder::buildUri()
            ->withUserInfo('username', 'password')
            ->withoutPassword()
            ->getUri();

        Assert::assertEquals('username', $uri->getUserInfo());
    }

    public function testBuilderWithHost()
    {
        $uri = UriBuilder::buildUri()
            ->withHost('example.org')
            ->getUri();

        Assert::assertEquals('example.org', $uri->getHost());
    }

    public function testClearingHost()
    {
        $uri = UriBuilder::buildUri()
            ->withHost('example.org')
            ->withoutHost()
            ->getUri();

        Assert::assertNull($uri->getHost());
    }

    public function testBuilderWithPort()
    {
        $uri = UriBuilder::buildUri()
            ->withPort(8080)
            ->getUri();

        Assert::assertEquals(8080, $uri->getPort());
    }

    public function testClearingPort()
    {
        $uri = UriBuilder::buildUri()
            ->withPort(8080)
            ->withoutPort()
            ->getUri();

        Assert::assertNull($uri->getPort());
    }

    public function testBuilderWithPath()
    {
        $uri = UriBuilder::buildUri()
            ->withPath('/path')
            ->getUri();

        Assert::assertEquals('/path', $uri->getPath());
    }

    /**
     * @throws PathResolverException
     */
    public function testBuilderWithResolvedPath()
    {
        $uri = UriBuilder::buildUri()
            ->withResolvedPath('/seg1/seg2/../seg3')
            ->getUri();

        Assert::assertEquals('/seg1/seg3', $uri->getPath());
    }

    /**
     * @throws PathResolverException
     */
    public function testBuilderWithPathResolvedWith()
    {
        $uri = UriBuilder::buildUri()
            ->withPath('/home/user1/')
            ->withExistingPathResolvedWith('../user2/downloads/')
            ->getUri();

        Assert::assertEquals('/home/user2/downloads/', $uri->getPath());
    }

    public function testClearingPath()
    {
        $uri = UriBuilder::buildUri()
            ->withPath('/path')
            ->withoutPath()
            ->getUri();

        Assert::assertNull($uri->getPath());
    }

    public function testBuilderWithQuery()
    {
        $uri = UriBuilder::buildUri()
            ->withQuery(['key' => 'value'])
            ->getUri();

        Assert::assertEquals(['key' => 'value'], $uri->getQuery());
    }

    public function testClearingQuery()
    {
        $uri = UriBuilder::buildUri()
            ->withQuery(['key' => 'value'])
            ->withoutQuery()
            ->getUri();

        Assert::assertEmpty($uri->getQuery());
    }

    public function testBuilderWithFragment()
    {
        $uri = UriBuilder::buildUri()
            ->withFragment('someFragment')
            ->getUri();

        Assert::assertEquals('someFragment', $uri->getFragment());
    }

    public function testClearingFragment()
    {
        $uri = UriBuilder::buildUri()
            ->withFragment('someFragment')
            ->withoutFragment()
            ->getUri();

        Assert::assertNull($uri->getFragment());
    }

    public function testParsingStringUri()
    {
        $stringUri = 'http://user:pass%2Fword@example.org:8080/path/to/file.png?key=value#top';
        $uri = UriBuilder::buildUri()
            ->withPartsFromStringUri($stringUri)
            ->getUri();

        $expectedUri = UriBuilder::buildUri()
            ->withScheme('http')
            ->withUserInfo('user', 'pass/word')
            ->withHost('example.org')
            ->withPort(8080)
            ->withPath('/path/to/file.png')
            ->withQuery(['key' => 'value'])
            ->withFragment('top')
            ->getUri();
        Assert::assertEquals($expectedUri, $uri);
    }

    public function testParsingStringUriWithoutQuery()
    {
        $stringUri = 'http://user:pass@example.org:8080/path/to/file.png#top';
        $uri = UriBuilder::buildUri()
            ->withPartsFromStringUri($stringUri)
            ->getUri();

        $expectedUri = UriBuilder::buildUri()
            ->withScheme('http')
            ->withUserInfo('user', 'pass')
            ->withHost('example.org')
            ->withPort(8080)
            ->withPath('/path/to/file.png')
            ->withFragment('top')
            ->getUri();
        Assert::assertEquals($expectedUri, $uri);
    }

    public function testParsingUriObject()
    {
        $expectedUri = UriBuilder::buildUri()
            ->withScheme('http')
            ->withUserInfo('user', 'pass')
            ->withHost('example.org')
            ->withPort(8080)
            ->withPath('/path/to/file.png')
            ->withQuery(['key' => 'value'])
            ->withFragment('top')
            ->getUri();

        $uri = UriBuilder::buildUri()
            ->withPartsFromUriObject($expectedUri)
            ->getUri();

        Assert::assertEquals($expectedUri, $uri);
    }

    /**
     * @throws SlugException
     */
    public function testBuilderWithSlugThrowExceptionWhenNoPathIsAvailable()
    {
        $this->expectException(SlugException::class);

        UriBuilder::buildUri()
            ->withSlug('file.ext');
    }

    /**
     * @param string $path
     * @param string $expected
     * @throws SlugException
     *
     * @dataProvider slugDataProvider
     */
    public function testBuilderWithSlugReplaceExistingSlug(string $path, string $expected)
    {
        //given
        $uri = UriBuilder::buildUri()
            ->withPath($path)
            ->withSlug('file2.ext')
            ->getUri();

        //when
        $result = $uri->getPath();

        //then
        Assert::assertEquals($expected, $result);
    }

    public function slugDataProvider(): array
    {
        return [
            'with dir slug' => [
                'path' => '/path/to/some/dir',
                'expected' => '/path/to/some/file2.ext',
            ],
            'with file slug' => [
                'path' => '/path/to/some/dir/file1.ext',
                'expected' => '/path/to/some/dir/file2.ext',
            ],
            'without slug' => [
                'path' => '/path/to/some/dir/',
                'expected' => '/path/to/some/dir/file2.ext',
            ],
        ];
    }

    /**
     * @param string $path
     * @param string $expected
     *
     * @dataProvider clearSlugDataProvider
     */
    public function testClearingSlug(string $path, string $expected)
    {
        //given
        $uri = UriBuilder::buildUri()
            ->withPath($path)
            ->withoutSlug()
            ->getUri();

        //when
        $result = $uri->getPath();

        //then
        Assert::assertEquals($expected, $result);
    }

    public function clearSlugDataProvider()
    {
        return [
            'with dir slug' => [
                'path' => '/path/to/some/dir',
                'expected' => '/path/to/some/',
            ],
            'with file slug' => [
                'path' => '/path/to/some/dir/file1.ext',
                'expected' => '/path/to/some/dir/',
            ],
            'without slug' => [
                'path' => '/path/to/some/dir/',
                'expected' => '/path/to/some/dir/',
            ],
            'without path' => [
                'path' => '',
                'expected' => '',
            ],
            'with root only' => [
                'path' => '/',
                'expected' => '/',
            ],
        ];
    }
}

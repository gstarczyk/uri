<?php

namespace Gstarczyk\Uri\Tests\Unit;

use Gstarczyk\Uri\PathResolver;
use Gstarczyk\Uri\PathResolverException;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class PathResolverTest extends TestCase
{
    /**
     * @param string $path
     * @param string $expected
     * @dataProvider dataProvider
     * @throws PathResolverException
     */
    public function testResolverResolveDotsInGivenPath(string $path, string $expected)
    {
        //given
        $resolver = new PathResolver();

        //when
        $result = $resolver->resolvePath($path);

        //then
        Assert::assertEquals($expected, $result);
    }

    public function dataProvider(): array
    {
        return [
            'absolute without dots' => [
                'path' => '/seg1/seg2/seg3/file.ext',
                'expected' => '/seg1/seg2/seg3/file.ext',
            ],
            'relative without dots' => [
                'path' => 'seg2/seg3/file.ext',
                'expected' => 'seg2/seg3/file.ext',
            ],
            'dot' => [
                'path' => '/seg1/seg2/./seg3/file.ext',
                'expected' => '/seg1/seg2/seg3/file.ext',
            ],
            'double dot' => [
                'path' => '/seg1/seg2/../seg3/file.ext',
                'expected' => '/seg1/seg3/file.ext',
            ],
            'mixed dots' => [
                'path' => '/seg1/./seg2/../seg3/file.ext',
                'expected' => '/seg1/seg3/file.ext',
            ],
            'complex dots' => [
                'path' => '/seg1/../seg2/../seg3/./seg4/seg5/../seg6/file.ext',
                'expected' => '/seg3/seg4/seg6/file.ext',
            ],
            'slug replacement' => [
                'path' => '/seg1/seg2/file1.ext/.././file2.ext',
                'expected' => '/seg1/seg2/file2.ext',
            ]
        ];
    }

    /**
     * @param string $path
     * @dataProvider invalidDataProvider
     * @throws PathResolverException
     */
    public function testResolverThrowExceptionWhenDotsCannotBeResolved(string $path)
    {
        //then
        $this->expectException(PathResolverException::class);

        //given
        $resolver = new PathResolver();

        //when
        $resolver->resolvePath($path);
    }

    public function invalidDataProvider(): array
    {
        return [
            'too much double dots' => [
                '/seg1/../../file2.ext'
            ],
        ];
    }

    /**
     * @param string $basePath
     * @param string $path
     * @throws PathResolverException
     *
     * @dataProvider incorrectPathsDataProvider
     */
    public function testResolvingTwoPathsThrowExceptionWhenPathsCannotBeResolved(string $basePath, string $path)
    {
        //then
        $this->expectException(PathResolverException::class);

        //given
        $resolver = new PathResolver();

        //when
        $resolver->resolvePaths($basePath, $path);
    }

    public function incorrectPathsDataProvider(): array
    {
        return [
            'unresolvable base path' => [
                'path1' => '../segA1/segA2',
                'path2' => 'segB1/segB2',
            ],
            'unresolvable paths' => [
                'path1' => '/segA1/segA2',
                'path2' => '../../../segB1/segB2',
            ],
        ];
    }

    /**
     * @param string $basePath
     * @param string $path
     * @param string $expected
     * @throws PathResolverException
     * @dataProvider pathsDataProvider
     */
    public function testResolvingTwoPaths(string $basePath, string $path, string $expected)
    {
        //given
        $resolver = new PathResolver();

        //when
        $result = $resolver->resolvePaths($basePath, $path);

        //then
        Assert::assertEquals($expected, $result);
    }

    public function pathsDataProvider(): array
    {
        return [
            'absolute with absolute' => [
                'path1' => '/segA1/segA2',
                'path2' => '/segB1/segB2',
                'expected' => '/segB1/segB2',
            ],
            'slug less absolute with absolute' => [
                'path1' => '/segA1/segA2/',
                'path2' => '/segB1/segB2',
                'expected' => '/segB1/segB2',
            ],
            'slug less absolute with slug less absolute' => [
                'path1' => '/segA1/segA2/',
                'path2' => '/segB1/segB2/',
                'expected' => '/segB1/segB2/',
            ],
            'absolute with relative' => [
                'path1' => '/segA1/segA2',
                'path2' => 'segB1/segB2',
                'expected' => '/segA1/segB1/segB2',
            ],
            'slug less absolute with relative' => [
                'path1' => '/segA1/segA2/',
                'path2' => 'segB1/segB2',
                'expected' => '/segA1/segA2/segB1/segB2',
            ],
            'absolute with ./path' => [
                'path1' => '/segA1/segA2',
                'path2' => './segB1',
                'expected' => '/segA1/segB1',
            ],
            'slug less absolute with ./path' => [
                'path1' => '/segA1/segA2/',
                'path2' => './segB1',
                'expected' => '/segA1/segA2/segB1',
            ],
            'absolute with ../path' => [
                'path1' => '/segA1/segA2',
                'path2' => '../segB1',
                'expected' => '/segB1',
            ],
            'slug less absolute with ../path' => [
                'path1' => '/segA1/segA2/',
                'path2' => '../segB1',
                'expected' => '/segA1/segB1',
            ],
            'absolute with slug' => [
                'path1' => '/segA1/segA2',
                'path2' => 'cover.png',
                'expected' => '/segA1/cover.png',
            ],
            'slug less absolute with slug' => [
                'path1' => '/segA1/segA2/',
                'path2' => 'cover.png',
                'expected' => '/segA1/segA2/cover.png',
            ],
            'relative with relative' => [
                'path1' => 'segA1/segA2',
                'path2' => 'segB1/segB2',
                'expected' => 'segA1/segB1/segB2',
            ],
            'slug less relative with relative' => [
                'path1' => 'segA1/segA2/',
                'path2' => 'segB1/segB2',
                'expected' => 'segA1/segA2/segB1/segB2',
            ],
            'dotted relative with relative' => [
                'path1' => './segA1/segA2',
                'path2' => 'segB1/segB2',
                'expected' => 'segA1/segB1/segB2',
            ],
            'dotted relative with dotted relative' => [
                'path1' => './segA1/segA2',
                'path2' => './segB1/segB2',
                'expected' => 'segA1/segB1/segB2',
            ],
            'double dotted relative with relative' => [
                'path1' => '/segA1/../segA2',
                'path2' => 'segB1/segB2',
                'expected' => '/segB1/segB2',
            ],
        ];
    }
}
